FROM node:wheezy
ARG source=./

ENV RABBIT_HOST=rabbit
ENV PULSEAUDIO_HOST=172.16.20.177
ENV LOGLVL=2
ENV VOICEAPIKEY=none
ENTRYPOINT node node-audio.js -k $VOICEAPIKEY
WORKDIR /app
RUN apt-get update && apt-get install mplayer -y
COPY $source . 


