var redis = require('redis');
var getEnv = require('./env.js');

// == REDIS HOST ==
var redisHost = getEnv('REDIS_HOST', '172.16.20.10');
var redisClient = redis.createClient(6379,redisHost);

// console.log("Play sound on bedroom");
// redisClient.publish("play_sound", JSON.stringify({
//      device: 0,
//      sound: 'computer_activate.mp3'
// }));

// console.log("Play text on bedroom");
// redisClient.publish("play_speech", JSON.stringify({
//      device: 1,
//      text: 'Das ist nur ein test im schlafzimmer.'
// }));

console.log("Play text on hallway");
redisClient.publish("play_speech", JSON.stringify({
     device: 1,
     text: 'Das ist nur ein test im Flur.'
}));

/*console.log("Play sound on living room");
redisClient.publish("play_sound", JSON.stringify({
     device: 2,
     sound: 'computer_activate.mp3'
}));

console.log("Play text on living room");
redisClient.publish("play_speech", JSON.stringify({
     device: 2,
     text: 'Das ist nur ein test im wohnzimmer.'
}));

console.log("Play sound on bed room");
redisClient.publish("play_sound", JSON.stringify({
     device: 2,
     sound: 'computer_activate.mp3'
}));

console.log("Play text on bed room");
redisClient.publish("play_speech", JSON.stringify({
     device: 2,
     text: 'Das ist nur ein test im wohnzimmer.'
}));*/
