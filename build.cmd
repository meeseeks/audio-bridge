git tag %1
git push --tags
docker rmi registry.gitlab.com/meeseeks/audio-bridge:latest -f

docker build -t registry.gitlab.com/meeseeks/audio-bridge:latest .
docker tag registry.gitlab.com/meeseeks/audio-bridge:latest registry.gitlab.com/meeseeks/audio-bridge:%1

docker push registry.gitlab.com/meeseeks/audio-bridge:latest
docker push registry.gitlab.com/meeseeks/audio-bridge:%1
