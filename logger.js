
module.exports = function(msg,lvl) {
    var logLevel = process.env.LOGLVL;

    //No loglevel requested from Env so go low
    if(typeof logLevel === 'undefined') { 
        logLevel = 0;
    }

    //Messages with no log level send in are considered info
    if(typeof lvl === 'undefined') { 
        lvl = 1;
    }

    //1 debug
    //2 info
    //3 warn
    //4 error
    if(lvl >= logLevel)
    {
        var currentdate = new Date(); 
        var datetime = currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

        var strlvl = "UKN";
        if(lvl === 1) { strlvl = "DBG"}
        if(lvl === 2) { strlvl = "IFO"}
        if(lvl === 3) { strlvl = "WRN"}
        if(lvl === 4) { strlvl = "ERR"}

        console.log("["+datetime+" | " + strlvl +"] " + msg)
    }
}
