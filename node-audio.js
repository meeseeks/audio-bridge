var exec = require('child_process').exec;
var request = require('request');
var fs = require('fs');
var crypto = require('crypto');
var getEnv = require('./env.js');
var amqp = require('amqplib/callback_api');

var argv = require('minimist')(process.argv.slice(2));

// == REDIS HOST ==
var rabbitHost = getEnv('RABBIT_HOST', 'scotty');
var pulseHost = getEnv('PULSEAUDIO_HOST', 'scotty');

if('h' in argv) {
        rabbitHost = argv["h"];
} else if('host' in argv)
{
        rabbitHost = argv["host"];
}

// == Speech Synthesis language ==
var speechLang = 'de-de';
if('l' in argv) {
        speechLang = argv["l"];
} else if('lang' in argv)
{
        speechLang = argv["lang"];
}

var voiceRssApiKey = 'none';
if('k' in argv) {
        voiceRssApiKey = argv["k"];
} else if('key' in argv)
{
        voiceRssApiKey = argv["key"];
}

var voiceEnabled = voiceRssApiKey !== 'none';

if(!voiceEnabled)
{
        console.warn('VoiceRSS API Key not given. Speech Syntehsis not available. go to www.voicerss.org to request a key and provide with -k.');
}

function mapDevice(devin)
{
        if(devin === 0) //Bedroom
        {
                return 0;
        }

        if(devin === 1) //hallwaz
        {
                return 1;
        }
        if(devin === 2) //Lounge
        {
                return 2;
        }
}

amqp.connect('amqp://'+ rabbitHost, function(err, conn) {
  conn.createChannel(function(err, ch) {
    
    ch.assertQueue("playsound",{durable:true},function(err,q) {
        ch.consume(q.queue, function(msg) {
                var sndMsg = JSON.parse(msg.content.toString());
                console.log("Play " + sndMsg.sound + " on device " + sndMsg.device);
                exec("mplayer -format s16le -srate 44100 -vo null -nolirc -ao pulse:" + pulseHost + ":" + mapDevice(sndMsg.device) + " /app/sounds/" + sndMsg.sound,cb);
      }, {noAck: true});
    });

    ch.assertQueue("playspeech",{durable:true},function(err,q) {
        ch.consume(q.queue, function(msg) {
                if(!voiceEnabled)
                {
                        console.warn('VoiceRSS API Key not given. Speech Syntehsis not available. go to www.voicerss.org to request a key and provide with -k.');
                        return;
                }
                var sndMsg = JSON.parse(msg.content.toString());
                console.log("Speech " + sndMsg.text + " on device " + sndMsg.device);

                var hash = crypto.createHash('md5').update(sndMsg.text).digest('hex');
                var filename = hash + '.mp3';

                var reqParams = {
                    'key':voiceRssApiKey,
                    'hl': speechLang,
                    'r':0,
                    'c':'mp3',
                    'f':'44khz_16bit_stereo',
                    'ssml':false,
                    'src':sndMsg.text
                };

                var stream = request({
                    method: 'POST',
                    uri: 'https://api.voicerss.org',
                    headers: {
                        host: 'api.voicerss.org',
                        'content-type': 'application/x-www-form-urlencoded' },
                        form: reqParams}
                    ).pipe(fs.createWriteStream(filename));

                stream.on('finish',function () {
                        exec("mplayer -format s16le -srate 44100 -vo null -nolirc -ao pulse:" + pulseHost + ":" + mapDevice(sndMsg.device) + " " + filename,cb);
                });
      }, {noAck: true});
    });

  });
});

var cb = function(err,stdout,stderr) {
        console.log(stdout);
        console.log(stderr);
}
console.log("Audio-bridge loaded");